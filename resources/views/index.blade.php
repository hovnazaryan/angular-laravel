<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Angular App</title>
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
    </head>

<body ng-app="app">

<div ui-view="header"></div>
<div ui-view="content" class="body"></div>
<div ui-view="footer"></div>

<script type="text/javascript" src="{{ asset('build/angular.js') }}"></script>
<script type="text/javascript" src="{{ asset('build/app.js') }}"></script>
</body>
</html>