<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $createdToken;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $createdToken)
    {
        $this->user = $user;
        $this->createdToken = $createdToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.forgotPassword' , ['user' => $this->user, 'userToken' => $this->createdToken]);
    }
}
