<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\EmailRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Mail\VerificationEmail;
use App\Mail\ForgotPasswordEmail;
use App\Models\PasswordReset;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'verify' , 'forgotPassword' ,'createNewPassword']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);

        $credentials = $credentials + ['activation_token' => null];

        if (! $token = auth()->attempt($credentials)) {
            $emailError = empty(User::query()
                ->whereNotNull('activation_token')
                ->whereEmail($credentials['email'])
                ->first()) ? 'Email or password incorrect.' : 'Please verify your account.';

            return response()->json(['errors' => ['email' => [$emailError]]], 422);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }


    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function register(RegisterRequest $request)
    {
        $createParams = array_merge($request->all(), ['activation_token' => str_random(128)]);
        $createdUser  = User::query()->create($createParams);

        if(!empty($createdUser)) {
            try {
                Mail::to($createdUser)->queue(new VerificationEmail($createdUser));
            } catch (\Exception $exception) {
                return response('Oops something went wrong.', 500);
            }
            return response()->json(['message' => 'Please verify your account.']);
        }

        return response('Empty createdUser.', 500);
    }


    /**
     * @param $token
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function verify($token)
    {
        $user = User::query()
            ->where('activation_token', '=', $token)
            ->first();


        if(!empty($user)) {
            if(! $user->update(['activation_token' => NULL]) ) {
                return response('Error during verification.', 500);
            }
            if( ! $token = auth()->login($user)) {
                return response('Error during login.', 500);
            }

            return response()->json(['message' => 'Successfully verified.' , 'access_token' => $token]);
        }

        return response()->json(['redirect'=>'/'] , 403);

    }

    public function forgotPassword(EmailRequest $request)
    {
       $email = $request['email'];

       $user = User::query()
            ->where('email', '=', $email)
            ->first();
        if(!empty($user)) {
            try {
                $createdToken = PasswordReset::query()->create(['email' => $email, 'token' => str_random(64)]);

                Mail::to($user)->queue(new ForgotPasswordEmail($user, $createdToken));
            } catch (\Exception $exception) {
                dd($exception->getMessage());
                return response('Oops something went wrong.', 500);
            }


            return response()->json(['message' => 'Check your email to reset your password']);
        }

        return response()->json(['message' => 'Invalid Email'] , 403);
    }

    public function createNewPassword(ResetPasswordRequest $request) {
        $email = PasswordReset::query()->where('token' , '=' , $request['token'])->first()->getAttribute('email');
        $user =  User::query()->where('email','=', $email)->first();
        $updated = $user->update(['password' => $request->input('password')]);
        if($updated) {
            return response()->json(['message' => 'Your password has been changed. Please Log In']);
        } else {
            return response()->json(['message' => 'Somethin went wrong , please try agail'] , 500);
        }

    }
}