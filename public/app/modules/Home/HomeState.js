app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('/', {
            url: "/",
            views : {
                'header' : {
                    templateUrl: "/app/modules/Home/views/header.html",
                    controller: 'HomeHeaderController'
                },
                'content' : {
                    templateUrl: "/app/modules/Home/views/index.html",
                    controller: 'HomeIndexController'
                },
                'footer' : {
                    templateUrl: "/app/modules/Home/views/footer.html",
                    controller: 'HomeFooterController'
                }
            }
        })

        .state('light', {
            url: "/light",
            views : {
                'header' : {
                    templateUrl: "/app/modules/Home/views/header-light.html",
                    controller: 'HomeHeaderLightController'
                },
                'content' : {
                    templateUrl: "/app/modules/Home/views/index.html",
                    controller: 'HomeIndexController'
                },
                'footer' : {
                    templateUrl: "/app/modules/Home/views/footer-light.html",
                    controller: 'HomeFooterLightController'
                }
            }
        })
});