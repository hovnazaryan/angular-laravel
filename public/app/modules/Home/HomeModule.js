app.controller('HomeHeaderController', function ($scope) {
    $scope.text = 'Header';
});

app.controller('HomeHeaderLightController', function ($scope) {
    $scope.text = 'Header-light';
});

app.controller('HomeIndexController', function ($scope) {
    $scope.text = 'Index';
});

app.controller('HomeFooterController', function ($scope) {
    $scope.text = 'Footer';
});

app.controller('HomeFooterLightController', function ($scope) {
    $scope.text = 'Footer';
});