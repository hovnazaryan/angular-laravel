app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('register', {
            url: "/register",
            views: {
                'header': {
                    templateUrl: "/app/modules/Header/views/index.html",
                    controller: "HeaderController"
                },
                'content': {
                    templateUrl: "/app/modules/Auth/views/register.html",
                    controller: "RegisterController"
                },
                'footer': {
                    templateUrl: "/app/modules/Footer/views/index.html",
                    controller : "HomeIndexController"
                }
            },
        })
        .state('login', {
            url: "/login",
            views: {
                'header': {
                    templateUrl: "/app/modules/Header/views/index.html",
                    controller: "HeaderController"
                },
                'content': {
                    templateUrl: "/app/modules/Auth/views/login.html",
                    controller: "LoginController"
                },
                'footer': {
                    templateUrl: "/app/modules/Footer/views/index.html",
                    controller : "HomeIndexController"
                }
            },
        })

        .state('logout', {
            url: "/logout",
            views: {
                'content': {
                    controller: "LogoutController"
                }
            },
        })

        .state('activate-profile', {
            url: "/activate-profile/:token",
            views: {
                'content': {
                    controller: "VerifyEmailController"
                }
            },
        })

        .state('forgotPassword', {
            url: "/forgotPassword",
            views: {
                'header': {
                    templateUrl: "/app/modules/Header/views/index.html",
                    controller: "HeaderController"
                },
                'content': {
                    templateUrl: "/app/modules/Auth/views/forgot_password.html",
                    controller: "ForgotPasswordController"
                },
                'footer': {
                    templateUrl: "/app/modules/Footer/views/index.html",
                    controller : "HomeIndexController"
                }
            },
        })

        .state('create-new-password', {
            url: "/create-new-password/:token",
            views: {
                'header': {
                    templateUrl: "/app/modules/Header/views/index.html",
                    controller: "HeaderController"
                },
                'content': {
                    templateUrl: "/app/modules/Auth/views/create-new-password.html",
                    controller: "CreateNewPasswordController"
                },
                'footer': {
                    templateUrl: "/app/modules/Footer/views/index.html",
                    controller : "HomeIndexController"
                }
            },
        })
});