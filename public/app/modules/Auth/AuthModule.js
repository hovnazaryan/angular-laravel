app.controller('RegisterController', function ($scope, $rootScope, AuthService, toastr) {
    $scope.user = {
        name: null,
        email: null,
        password: null,
        password_confirmation: null
    };

    $scope.message = null;
    $scope.errors = {};

    $scope.register = () => {
        AuthService.register($scope.user, (res) => {
            $scope.message = res.message;
            $scope.user = {
                name : null,
                email : null,
                password : null,
                password_confirmation: null
            };
            $scope.errors = {};

        }, (err) => {
            toastr.error('Please fix errors');
            $scope.errors = err.data.errors;
        })
    }
});

app.controller('LogoutController', function ($state, authManager) {
    localStorage.removeItem('jwtToken');
    authManager.unauthenticate();
    $state.go('login');
});

app.controller('LoginController', function ($scope, AuthService, toastr, $state) {
    $scope.user = {
        email: null,
        password: null
    };

    $scope.errors = {};
    $scope.login = () => {
        AuthService.login($scope.user, function (res) {
            toastr.success('Successfully logged in.');
            localStorage.setItem('jwtToken', res.access_token);
            $state.go('/');
        }, function (err) {
            toastr.error('Please fix errors.');
            $scope.errors = err.data.errors;
        })
    }
});


app.controller('VerifyEmailController', function ($stateParams, AuthService, toastr, $state, authManager) {
    AuthService.verifyUser({token :  $stateParams.token}, (res) => {
        toastr.success(res.message);
        authManager.authenticate();
        localStorage.setItem('jwtToken', res.access_token);
        $state.go('/');
    }, (err) => {
        $state.go('login');
    });
});

app.controller('ForgotPasswordController' , function (AuthService , toastr , $scope , $rootScope) {
  $scope.forgotPassword = () => {
      AuthService.forgotPassword({email : $scope.email}, (res) => {
          $scope.errors = {};
          $scope.message = res.message;
      }, (err) => {
          $scope.errors = err.data.errors;
          if(err.data.message && typeof err.data.errors === 'undefined') {
              alert(err.data.message);
              return;
          }
          toastr.error('Please fix errors');
      });
  }
});

app.controller('CreateNewPasswordController' , function (AuthService , toastr , $scope, $rootScope, $stateParams, $state) {
   $scope.createNewPassword = () => {
       AuthService.createNewPassword({
           password: $scope.password ,
           password_confirmation: $scope.password_confirmation,
           token : $stateParams.token}, (res) => {
                toastr.success(res.message);
                $state.go('login');
       }, (err) => {
            toastr.error(err)
       })
   }
});